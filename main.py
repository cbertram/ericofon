from country_codes import country_codes
from gpiozero import Button
from time import monotonic, sleep
from enum import Enum
import subprocess
import requests
import json
from random import random

def shutdown_now():
  subprocess.Popen(["sudo", "shutdown", "now"])

sound_process = None
def terminate_sound():
  global sound_process
  if sound_process:
    sound_process.terminate()
    sound_process = None
def play_shutdown():
  global sound_process
  terminate_sound()
  sound_process = subprocess.Popen(["mpv", "--no-video", "--audio-channels=1", "./shutdown.mp3"])
def play_dial_tone():
  global sound_process
  terminate_sound()
  sound_process = subprocess.Popen(["mpv", "--no-video", "--audio-channels=1", "./dial_tone.mp3"])
def play_ringback():
  global sound_process
  terminate_sound()
  sound_process = subprocess.Popen(["mpv", "--no-video", "--audio-channels=1", "--loop=inf", "./ringback.mp3"])
def play_busy():
  global sound_process
  terminate_sound()
  sound_process = subprocess.Popen(["mpv", "--no-video", "--audio-channels=1", "--loop=inf", "./busy.mp3"])
def play_radiooooo(song_url):
  global sound_process
  terminate_sound()
  sound_process = subprocess.Popen(["mpv", "--no-video", "--audio-channels=1", song_url])

class State(Enum):
  ON_HOOK = 0
  OFF_HOOK = 1
  DIALING = 2
  RINGING = 3
  BUSY = 4
  ANSWERED = 5
  SHUTDOWN = 6

state = State.ON_HOOK
new_state = True
def set_state(s):
  global state, new_state
  state = s
  new_state = True

button = Button(3, bounce_time=0.05)

def handle_button_press():
  set_state(State.OFF_HOOK)
def handle_button_release():
  set_state(State.ON_HOOK)

button.when_pressed = handle_button_press
button.when_released = handle_button_release

if button.is_pressed: # fix state at startup
  handle_button_press()
else:
  handle_button_release()

dial = Button(4, bounce_time=0.01)
dial_count = 0
last_dial_release = monotonic()

def handle_dial_release():
  global dial_count, last_dial_release
  dial_count += 1
  last_dial_release = monotonic()

dial.when_released = handle_dial_release

def loop():
  global state, new_state, dial_count, last_dial_release

  loop_sleep = 0.05
  dialed = ""
  song_url = None
  last_on_hook = monotonic()

  while True:
    match state:
      case State.ON_HOOK:
        if new_state:
          print("State: ON_HOOK")
          last_on_hook = monotonic()
          terminate_sound()
          new_state = False
        if monotonic() - last_on_hook > 60*60:
          set_state(State.SHUTDOWN)

      case State.OFF_HOOK:
        if new_state:
          print("State: OFF_HOOK")
          new_state = False
        set_state(State.DIALING)

      case State.DIALING:
        if new_state:
          print("State: DIALING")
          play_dial_tone()
          dial_count = 0
          dialed = ""
          new_state = False
        if monotonic() - last_dial_release > 0.2 and dial_count > 0:
          print(dial_count)
          dialed += str(dial_count % 10)
          dial_count = 0
          terminate_sound()
        if dialed == '0000':
          play_shutdown()
          sleep(3)
          set_state(State.SHUTDOWN)
        elif dialed[:2] == '00':
          if dialed[2:-4] in country_codes:
            set_state(State.RINGING)
        elif len(dialed) == 4:
          set_state(State.RINGING)

      case State.RINGING:
        if new_state:
          print("State: RINGING")
          play_ringback()
          print(dialed)
          country_code = dialed[2:-4] or '45'
          isocodes = country_codes[country_code]
          print(isocodes)
          year = dialed[-4:-1]+'0' # year must be multiple of 10 for radiooooo
          print(year)
          response = requests.post(
            "https://radiooooo.com/play",
            headers={
              "Content-Type": "application/json",
            },
            data=json.dumps({
              "mode": "explore",
              "moods": ["FAST", "SLOW"],
              "decades": [year],
              "isocodes": isocodes,
            })
          )
          if state != State.RINGING:
            continue
          print(response.text)
          print(response.status_code)
          try:
            response_json = response.json()
            song_url = response_json.get("links", {}).get("mpeg")
          except:
            song_url = None
          new_state = False
        expected_ring_time = 7
        if random() < loop_sleep/7:
          if song_url:
            set_state(State.ANSWERED)
          else:
            set_state(State.BUSY)

      case State.BUSY:
        if new_state:
          print("State: BUSY")
          play_busy()
          new_state = False

      case State.ANSWERED:
        if new_state:
          print("State: ANSWERED")
          print(song_url)
          play_radiooooo(song_url)
          new_state = False

      case State.SHUTDOWN:
        if new_state:
          print("State: SHUTDOWN")
          shutdown_now()
          new_state = False

    sleep(loop_sleep)

loop()
